package com.example.PDFTextFinder;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PDFTextFinder {
    public static void main(String[] args) throws IOException {

            List<String> desiredWords = new ArrayList<String>();
            desiredWords.add("portokali");
            desiredWords.add("strings");
            desiredWords.add("Apache");

        Map<String, Boolean> wordsFoundMap = findTextInPDFDocument("C:/Users/john/Desktop/uzidx-4dnut.pdf", desiredWords);
        System.out.println("MAP: " + wordsFoundMap.toString());

    }
         public static Map<String, Boolean> findTextInPDFDocument(String path, List<String> words) throws IOException {
             File inputPdfFile = new File(path);
             PDDocument pdfFile = PDDocument.load(inputPdfFile);
             PDFTextStripper tStripper = new PDFTextStripper();

             final String pdfFileInText = tStripper.getText(pdfFile);

             Map<String, Boolean> wordsMap = new HashMap<String, Boolean>();
             for(int i = 0; i< words.size(); i++){
                 if(pdfFileInText.contains(words.get(i))){
                     wordsMap.put(words.get(i), true);
                 }else{
                     wordsMap.put(words.get(i), false);
                 }
             }
             return wordsMap;
         }


}
package com.example;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import static com.example.MSWordTextFinder2.findTextInWordDocument;
import static com.example.PDFTextFinder.PDFTextFinder.findTextInPDFDocument;

public class PdfWordFinder {

    public static String getFileExtension(String path) {
        String fileName = new File(path).getName();
        int dotIndex = fileName.lastIndexOf('.');
        return (dotIndex == -1) ? "" : fileName.substring(dotIndex + 1);
    }


    public static void main(String args[]) throws IOException {

        System.out.println("Give the number of words to be found: ");
        Scanner NumofWords = new Scanner(System.in);
        String noWords = NumofWords.nextLine();

        int no = Integer.parseInt(noWords);
        List<String> desiredWords = new ArrayList<String>();

        for (int i = 0; i < no; i++){
            System.out.println("Enter word: ");
            Scanner inputWord = new Scanner(System.in);
            String StrWord = inputWord.nextLine();
            desiredWords.add(StrWord);
        }

        //Enter the path of file
        System.out.println("Give the path: ");
        Scanner inputPath = new Scanner(System.in);
        String pathInput = inputPath.nextLine();

        String path_dir = getFileExtension(pathInput);

        if (path_dir.equals("pdf")){
            Map<String, Boolean> wordsFoundMap = findTextInPDFDocument(pathInput, desiredWords);
            System.out.println("MAP pdf: " + wordsFoundMap.toString());

        }else if(path_dir.equals("docx")){
            Map<String, Boolean> wordsFoundMap = findTextInWordDocument(pathInput, desiredWords);
            System.out.println("MAP docx: " + wordsFoundMap.toString());

        }else{
            System.out.println("invalid type of file");
        }

    }
}

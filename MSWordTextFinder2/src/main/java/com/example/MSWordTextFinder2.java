package com.example;

import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MSWordTextFinder2 {
    // create a method where inputs the path and a list of words
    // outputs a map with the words if found true or false

    public static Map<String, Boolean> findTextInWordDocument(String path, List<String> words) throws IOException {

        // opens the file from the path

        FileInputStream input_text = new FileInputStream(path);
        XWPFDocument wordFile = new XWPFDocument(input_text);
        XWPFWordExtractor wordToText = new XWPFWordExtractor(wordFile);
        String TextFromWord = wordToText.getText();

        // creates the map for the output

        Map<String, Boolean> wordsMap = new HashMap<String, Boolean>();

        for(int i = 0; i< words.size(); i++){
            if(TextFromWord.contains(words.get(i))){
                wordsMap.put(words.get(i), true);
            }else{
                wordsMap.put(words.get(i), false);
            }
        }
        return wordsMap;
    }

    public static void main(String args[]) throws IOException {

        List<String> desiredWords = new ArrayList<String>();
        desiredWords.add("portokali");
        desiredWords.add("strings");
        desiredWords.add("Apache");

        Map<String, Boolean> wordsFoundMap = findTextInWordDocument("C:/Users/john/Desktop/uzidx-4dnut-converted.docx", desiredWords);
        System.out.println("MAP: " + wordsFoundMap.toString());
    }
}
